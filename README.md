# colectie_scan

## Content

```
./Albert Camus:
Albert Camus - Actuale.pdf
Albert Camus - Mitul lui Sisif.pdf

./Arthur Schopenhauer:
Arthur Schopenhauer - Aforisme asupra intelepciunii in viata.pdf
Arthur Schopenhauer - Arta de a avea intotdeauna dreptate.pdf
Arthur Schopenhauer - Despre impatrita radacina a principiului ratiunii suficiente.pdf
Arthur Schopenhauer - Eseu despre liberul arbitru.pdf
Arthur Schopenhauer - Fundamentele Moralei.pdf
Arthur Schopenhauer - Lumea ca vointa si reprezentare, Vol. 1.pdf
Arthur Schopenhauer - Lumea ca vointa si reprezentare, Vol. 2.pdf
Arthur Schopenhauer - Lumea ca vointa si reprezentare, Vol. 3.pdf
Arthur Schopenhauer - Parerga si paralipomena.pdf
Arthur Schopenhauer - Viata amorul moartea.pdf

./Emil Cioran:
Emil Cioran - Amurgul gindurilor.pdf
Emil Cioran - Antologia Portertului.pdf
Emil Cioran - Caiete I.pdf
Emil Cioran - Caiete II.pdf
Emil Cioran - Caiete III.pdf
Emil Cioran - Cartea amagirilor.pdf
Emil Cioran - Demiurgul cel Rau.pdf
Emil Cioran - Despre neajunsul de a te fi nascut.pdf
Emil Cioran - Eseuri.pdf
Emil Cioran - Exercitii de admiratie.pdf
Emil Cioran - Indreptar patimas.pdf
Emil Cioran - Ispita de a exista.pdf
Emil Cioran - Istorie si utopie.pdf
Emil Cioran - Lacrimi si sfinti.pdf
Emil Cioran - Marturisiri si anateme.pdf
Emil Cioran - Pe culmile disperarii.pdf
Emil Cioran - Razne.pdf
Emil Cioran - Revelatiile durerii.pdf
Emil Cioran - Scrisori catre cei de-acasa.pdf
Emil Cioran - Silogismele amaraciunii.pdf
Emil Cioran - Tratat de descompunere.pdf

./Fernando Pessoa:
Fernando Pessoa - Cartea nelinistirii.pdf

./Francesco Petrarca:
Francesco Petrarca - Cantonierul.pdf

./Fyodor Mikhailovich Dostoevsky:
Fyodor Mikhailovich Dostoevsky - Jurnal de scriitor.pdf
Fyodor Mikhailovich Dostoevsky - Opere Vol. 01.djvu
Fyodor Mikhailovich Dostoevsky - Opere Vol. 02.pdf
Fyodor Mikhailovich Dostoevsky - Opere Vol. 03.djvu
Fyodor Mikhailovich Dostoevsky - Opere Vol. 04.djvu
Fyodor Mikhailovich Dostoevsky - Opere Vol. 05.djvu
Fyodor Mikhailovich Dostoevsky - Opere Vol. 06.djvu
Fyodor Mikhailovich Dostoevsky - Opere Vol. 07.djvu
Fyodor Mikhailovich Dostoevsky - Opere Vol. 08.djvu
Fyodor Mikhailovich Dostoevsky - Opere Vol. 09.djvu
Fyodor Mikhailovich Dostoevsky - Opere Vol. 10.djvu
Fyodor Mikhailovich Dostoevsky - Opere Vol. 11.djvu

./Hermann Hesse:
Hermann Hesse - Knulp & Demian.pdf
Hermann Hesse - Peter Camenzind.pdf

./Johann Wolfgang von Goethe:
Johann Wolfgang von Goethe - Opere Vol. 01.pdf
Johann Wolfgang von Goethe - Opere Vol. 02.pdf
Johann Wolfgang von Goethe - Opere Vol. 03.pdf
Johann Wolfgang von Goethe - Opere Vol. 04.pdf
Johann Wolfgang von Goethe - Opere Vol. 05.pdf
Johann Wolfgang von Goethe - Opere Vol. 06.pdf
Johann Wolfgang von Goethe - Opere Vol. 07.pdf
Johann Wolfgang von Goethe - Opere Vol. 08.pdf

./Joseph Campbell:
Joseph Campbell - Eroul cu o mie de chipuri.pdf

./Niccolo Machiavelli:
Niccolo Machiavelli - Principele.pdf

./Romain Gary (Emile Ajar):
Romain Gary (Emile Ajar) - Regele Solomon.pdf

./Shusaku Endo:
Shusaku Endo - Tacere.pdf

./Victor Hugo:
Victor Hugo - Anul 93.pdf
Victor Hugo - Cea din urma zi a unui condamnat.pdf
Victor Hugo - Han din Islanda.pdf
Victor Hugo - Legenda Secolelor.pdf
Victor Hugo - Marion Delorme & Hernani.pdf
Victor Hugo - Notre-Dame de Paris [Adevarul].pdf
Victor Hugo - Notre-Dame de Paris Vol. 01.pdf
Victor Hugo - Notre-Dame de Paris Vol. 02.pdf
Victor Hugo - Oamenii marii.pdf
Victor Hugo - Omul care rade.pdf
Victor Hugo - Regele petrece & Ruy Blas.pdf
Victor Hugo - Rinul.pdf
Victor Hugo - Scrisori din calatorie.pdf
Victor Hugo - Ultima zi a unui condamnant la moarte & Bug-Jargal.pdf
```

